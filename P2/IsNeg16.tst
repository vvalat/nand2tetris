// Testing custom IsNeg16

load IsNeg16.hdl,
output-file IsNeg16.out,
compare-to IsNeg16.cmp,
output-list in%B1.16.1 out%B3.1.3;

set in %B0000000000000000,
eval,
output;

set in %B1111111111111111,
eval,
output;

set in %B0011101001100001,
eval,
output;

set in %B1000010110010101,
eval,
output;
