// Testing custom IsNull16

load IsNull16.hdl,
output-file IsNull16.out,
compare-to IsNull16.cmp,
output-list in%B1.16.1 out%B3.1.3;

set in %B0000000000000000,
eval,
output;

set in %B1111111111111111,
eval,
output;

set in %B0000000000000001,
eval,
output;

set in %B1000000000000000,
eval,
output;
